package prova;
import javafx.scene.shape.SVGPath;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class MySVGPath extends SVGPath {

	private final Tooltip TP;
	private  Paint FILL;
	
	public MySVGPath(String[] info) {
		TP = new Tooltip();
		initInfo(info[0].split(":"));
		Tooltip.install(this, TP);
		setContent(info[1]);
		setStyle("-fx-stroke: black;");
	}
	
	
	private void initInfo(String[] info) {
		this.TP.setText(info[0]);
		this.setFill(FILL = Color.web(info[2]));
	}
	
	protected Paint getColor() {
		return this.FILL;
	}

}
