package prova;

import java.io.IOException;

//import java.util.Optional;

import com.jfoenix.controls.JFXButton;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainMenù extends Application {
	
	private final double width = 400;
	private final double height = 600;
	
	public void start(Stage stage){
		VBox vb = new VBox(20);
	    BorderPane bp = new BorderPane();
		Scene scene = new Scene(bp, width , height);
		stage.setScene(scene);
	    stage.setTitle("Risiko!!");
	    scene.getStylesheets().add(MainMenù.class.getResource("css/style.css").toExternalForm());    
	    
	    JFXButton start = new JFXButton("New Game");
	    start.getStyleClass().add("button-raised");
	    JFXButton load = new JFXButton("Load Game");
	    load.getStyleClass().add("button-raised");
	    JFXButton credit = new JFXButton("Credit");
	    credit.getStyleClass().add("button-raised");
	    JFXButton guide = new JFXButton("Game Rules");
	    guide.getStyleClass().add("button-raised");
	    JFXButton exit = new JFXButton("Exit");
	    exit.getStyleClass().add("button-raised");
	    
	    start.setOnAction(e -> {
	    	Platform.runLater(new Runnable() {
	    	       public void run() {             
	    	           try {
						new worldMap().start(new Stage());
					} catch (IOException e) {
						e.printStackTrace();
					}
	    	       }
	    	});
	    	stage.close();
	    });
	    load.setOnAction(e -> {
	    	Alert alert = new Alert(AlertType.ERROR);
	    	alert.setTitle("Error!");
	    	alert.setHeaderText("Warning! An error ocurred!");
	    	alert.setContentText("This method isn't implemented yet!");
	    	alert.show();
	    });
	    credit.setOnAction(e -> System.out.println("dcesplodo!"));
	    guide.setOnAction(e -> System.out.println("Google is your friend!"));
//	    exit.setOnAction(e -> {
//	    	Alert alert = new Alert(AlertType.CONFIRMATION);
//	    	alert.setTitle("Are you sure?");
//	    	alert.setHeaderText("Are you sure you want to exit game?");
//	    	alert.setContentText("Pick one:");
//	    	
//	    	Optional<ButtonType> result = alert.showAndWait();
//	    	if(result.isPresent() && result.get() == ButtonType.OK){
//	    		System.exit(0);
//	    	} else {
//	    		alert.close();
//	    	}
//	    });
	    ImageView imgv = new ImageView("file:res/tank-m.png");
	    
	    vb.getChildren().addAll(imgv, start, load, credit, guide, exit);
	    vb.setAlignment(Pos.CENTER);
	    bp.setCenter(vb);
	    bp.setBackground(new Background(new BackgroundImage(new Image("file:res/bg.gif"),null,null,null, null)));
	    stage.getIcons().add(new Image("file:res/tank.png"));
	    stage.show();
	}
	
public static void main(String[] args)	{
	Application.launch(args);
}
	
}
