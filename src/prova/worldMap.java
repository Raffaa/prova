package prova;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.jfoenix.controls.JFXButton;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.stage.Screen;

public class worldMap extends Application {
	
  @Override
  public void start(Stage stage) throws IOException {
	  
	final double SCALE = 0.7;
	final double width = Screen.getPrimary().getBounds().getWidth() * SCALE;
	final double height = Screen.getPrimary().getBounds().getHeight() * SCALE;
	final Path filePath = Paths.get("res/RiskClassicMap.txt");
	final Stream<String> Filelines = Files.lines(filePath);
	final BorderPane borderpane = new BorderPane();	
    final Pane world = new Pane();
    final HBox hBoxFooter = new HBox();
    final HBox hBoxHeader = new HBox();
    Scene scene = new Scene(borderpane, width , height);
    
    Label attCountry = new Label("PROVA");
    attCountry.setMaxWidth(Double.MAX_VALUE);
    Label difCountry = new Label("PROVA");
    difCountry.setMaxWidth(Double.MAX_VALUE);
    ImageView attDice = new ImageView();
    ImageView difDice = new ImageView();
    JFXButton battle = new JFXButton();
    battle.setGraphic(new ImageView("file:res/battle.png"));
    battle.setMaxWidth(Double.MAX_VALUE);
    
    
    hBoxFooter.setAlignment(Pos.CENTER);
    hBoxFooter.getChildren().addAll(attCountry, attDice, battle, difDice, difCountry);
    HBox.setHgrow(attCountry, Priority.ALWAYS);
    HBox.setHgrow(attDice, Priority.ALWAYS);
    HBox.setHgrow(battle, Priority.ALWAYS);
    HBox.setHgrow(difDice, Priority.ALWAYS);
    HBox.setHgrow(difCountry, Priority.ALWAYS);
    hBoxFooter.setStyle("-fx-background-color: DAE6F3;");   
    stage.setScene(scene);
    stage.setTitle("Risiko!! - GamePlay");
    hBoxFooter.setMaxHeight(10);
    Filelines.forEach((line) -> {
    	MySVGPath svg = new MySVGPath(line.split("!"));
    	svg.setOnMouseEntered(e -> svg.setFill(Color.web("#a6a6a6")));
    	svg.setOnMouseExited(e -> svg.setFill(svg.getColor()));
    	svg.setOnMouseClicked(e -> System.out.println("Click!"));

    	world.getChildren().add(svg);
    });	
	world.getTransforms().add(new Scale(width/(16*250),height/(9*250),0,0));
    borderpane.setCenter(world);
    borderpane.setBottom(hBoxFooter);
    Filelines.close();
    borderpane.setBackground(new Background(new BackgroundFill(Color.web("#505062"),null,null)));
    stage.getIcons().add(new Image("file:res/tank.png"));
    stage.show();
  }
}


